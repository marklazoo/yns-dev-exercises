<!DOCTYPE html>
<html>
<title>
    Advanced SQL 7-1
</title>
<head>
    <style>
        #sql 
        {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #sql td, #sql th 
        {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #sql tr:nth-child(even){background-color: #f2f2f2;}

        #sql tr:hover {background-color: #ddd;}

        #sql th 
        {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #1a8cff;
          color: white;
        }

        .header
        {
            text-align: center;
            padding: 6px 6px 6px 6px;
            margin: 6px 2px 6px 2px;
        }

    </style>
</head>
<body>
    <h2 class="header">
        Advanced SQL 7-1: Selecting by age using birth date
    </h2>
<?php 
    $con = mysqli_connect("localhost", "root","") or die ("Cannot connect!");
    $database = mysqli_select_db($con, "yns_training") or die('Cannot Access Database');
    $date = date('Y-m-d');
    $que1 = mysqli_query($con,"select * from employees where FLOOR(DATEDIFF('$date', birthdate)/365)>30 AND FLOOR(DATEDIFF('$date', birthdate)/365)<40");
?>
    <table id="sql">
    	<tr align=center>
    		<th> <b>id</b> </th>
    		<th> <b>first_name</b> </th>
    		<th> <b>last_name</b> </th>
    		<th> <b>middle_name</b> </th>
            <th> <b>birthdate</b> </th>
    		<th> <b>department_id</b> </th>
    		<th> <b>hire_date</b> </th>
    		<th> <b>boss_id</b> </th>
    	</tr>
    	<?php
    		while($row1=mysqli_fetch_assoc($que1))
    		{
    	?>
    		<tr align=center>
    			<td width=10%> <?php echo $row1['id']; ?> </td>
    			<td width=10%> <?php echo $row1['first_name']; ?> </td>
    			<td width=10%> <?php echo $row1['last_name']; ?> </td>
    			<td width=10%> <?php echo $row1['middle_name']; ?> </td>
                <td width=10%> <?php echo $row1['birthdate']; ?> </td>
    			<td width=10%> <?php echo $row1['department_id']; ?> </td>
    			<td width=10%> <?php echo $row1['hire_date']; ?> </td>
    			<td width=10%> <?php echo $row1['boss_id']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>	
</body>
</html>