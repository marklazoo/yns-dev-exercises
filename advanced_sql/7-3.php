<!DOCTYPE html>
<html>
<title>
    Advanced SQL 7-3
</title>
<head>
    <style>
        #sql 
        {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #sql td, #sql th 
        {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #sql tr:nth-child(even){background-color: #f2f2f2;}

        #sql tr:hover {background-color: #ddd;}

        #sql th 
        {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #1a8cff;
          color: white;
        }

        .header
        {
            text-align: center;
            padding: 6px 6px 6px 6px;
            margin: 6px 2px 6px 2px;
        }

    </style>
</head>
<body>
    <h2 class="header">
        Advanced SQL 7-3: Using Case Conditional Statement
    </h2>
<?php 
    $con = mysqli_connect("localhost", "root","") or die ("Cannot connect!");
    $database = mysqli_select_db($con, "yns_training") or die('Cannot Access Database');

    $queCase = mysqli_query($con,"select *, CASE WHEN name='CEO' THEN 'Chief Executive Officer' WHEN name='CTO' THEN 'Chief Technical Officer' WHEN name='CFO' THEN 'Chief Financial Officer' ELSE name END as 'fullText' from positions INNER JOIN employee_positions ON employee_positions.position_id = positions.id INNER JOIN employees ON employees.id = employee_positions.employee_id");
?>
    
 
	<table id="sql">
    	<tr align=center>
    		<th> <b>id</b> </th>
            <th> <b>first_name</b> </th>
            <th> <b>middle_name</b> </th>
            <th> <b>last_name</b> </th>
            <th> <b>birth_date</b> </th>
            <th> <b>department_id</b> </th>
            <th> <b>hire_date</b> </th>
            <th> <b>boss_id</b> </th>
            <th> <b>Position</b> </th>
    	</tr>
    	<?php
    		while($rowCase=mysqli_fetch_assoc($queCase))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $rowCase['id']; ?> </td>
                <td> <?php echo $rowCase['first_name']; ?> </td>
                <td> <?php echo $rowCase['middle_name']; ?> </td>
                <td> <?php echo $rowCase['last_name']; ?> </td>
                <td> <?php echo $rowCase['birthdate']; ?> </td>
                <td> <?php echo $rowCase['department_id']; ?> </td>
                <td> <?php echo $rowCase['hire_date']; ?> </td>
                <td> <?php echo $rowCase['boss_id']; ?> </td>
                <td> <?php echo $rowCase['fullText']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>
</body>
</html>

