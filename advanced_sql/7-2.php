<!DOCTYPE html>
<html>
<title>
    Advanced SQL 7-2
</title>
<head>
    <style>
        #sql 
        {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #sql td, #sql th 
        {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #sql tr:nth-child(even){background-color: #f2f2f2;}

        #sql tr:hover {background-color: #ddd;}

        #sql th 
        {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #1a8cff;
          color: white;
        }

        .header
        {
            text-align: center;
            padding: 6px 6px 6px 6px;
            margin: 6px 2px 6px 2px;
        }

    </style>
</head>
<body>
    <h2 class="header">
        Advanced SQL 7-2: List all therapists according to their daily work shifts
    </h2>
<?php 
    $con = mysqli_connect("localhost", "root","") or die ("Cannot connect!");
    $database = mysqli_select_db($con, "exercisessql") or die('Cannot Access Database');

    $queTherapists = mysqli_query($con,"select therapists.name, daily_work_shifts.therapist_id, daily_work_shifts.target_date, daily_work_shifts.start_time, daily_work_shifts.end_time, CASE WHEN daily_work_shifts.start_time <= '05:59:59' AND daily_work_shifts.start_time >= '00:00:00' THEN CONCAT(CAST(daily_work_shifts.target_date + 1 as date),' ', daily_work_shifts.start_time) ELSE CONCAT(daily_work_shifts.target_date,' ', daily_work_shifts.start_time) END as sort_start_time from daily_work_shifts INNER JOIN therapists ON daily_work_shifts.therapist_id = therapists.id ORDER BY daily_work_shifts.target_date, sort_start_time ASC");
?>
    
 
	<table id="sql">
    	<tr align=center>
    		<th> <b>name</b> </th>
            <th> <b>therapist_id</b> </th>
            <th> <b>target_date</b> </th>
            <th> <b>start_time</b> </th>
            <th> <b>end_time</b> </th>
            <th> <b>sort_start_time</b> </th>
    	</tr>
    	<?php
    		while($rowTherapists=mysqli_fetch_assoc($queTherapists))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $rowTherapists['name']; ?> </td>
                <td> <?php echo $rowTherapists['therapist_id']; ?> </td>
                <td> <?php echo $rowTherapists['target_date']; ?> </td>
                <td> <?php echo $rowTherapists['start_time']; ?> </td>
                <td> <?php echo $rowTherapists['end_time']; ?> </td>
                <td> <?php echo $rowTherapists['sort_start_time']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>
</body>
</html>

