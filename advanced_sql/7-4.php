<!DOCTYPE html>
<html>
<title>
    Advanced SQL 7-4
</title>
<head>
    <style>
        #sql 
        {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 25%;
        }

        #sql td, #sql th 
        {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #sql tr:nth-child(even){background-color: #f2f2f2;}

        #sql tr:hover {background-color: #ddd;}

        #sql th 
        {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #1a8cff;
          color: white;
        }

        .header
        {
            text-align: center;
            padding: 6px 6px 6px 6px;
            margin: 6px 2px 6px 2px;
        }

    </style>
</head>
<body>
    <h2 class="header">
        Advanced SQL 7-4: Create sql that will display the OUTPUT
    </h2>
<?php 
    $con = mysqli_connect("localhost", "root","") or die ("Cannot connect!");
    $database = mysqli_select_db($con, "exercisessql") or die('Cannot Access Database');

    $queOriginal = mysqli_query($con,"select * from advancedsql");
    $queNew = mysqli_query($con,"select *, CASE WHEN parent_id IS NULL THEN id ELSE parent_id END as newOrder from advancedsql ORDER BY newOrder, id");
?>
    
    <center>
    <h4>Table Master Data</h4>
    	<table id="sql">
        	<tr align=center>
        		<th> <b>id</b> </th>
                <th> <b>parent_id</b> </th>
        	</tr>
        	<?php
        		while($rowOrig=mysqli_fetch_assoc($queOriginal))
        		{
        	?>
        		<tr align=center>
        			<td> <?php echo $rowOrig['id']; ?> </td>
                    <td> <?php echo $rowOrig['parent_id']; ?> </td>
        		</tr>
        	<?php
        		}
        	?>
        </table><br>

        <h4>OUTPUT</h4>
        
        <table id="sql">
            <tr align=center>
                <th> <b>id</b> </th>
                <th> <b>parent_id</b> </th>
            </tr>
            <?php
                while($rowNew=mysqli_fetch_assoc($queNew))
                {
            ?>
                <tr align=center>
                    <td> <?php echo $rowNew['id']; ?> </td>
                    <td> <?php echo $rowNew['parent_id']; ?> </td>
                </tr>
            <?php
                }
            ?>
        </table><br>
    </center>

</body>
</html>

