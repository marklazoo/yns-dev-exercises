<?php
	session_start();
	session_destroy();
?> 

<!DOCTYPE html>
<html>
<style type="text/css">
	#sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:nth-child(even){
    	background-color: #f2f2f2;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		margin: 10px 10px;
		cursor: pointer;
		width: 25%;
	}

	input[type=email], select {
		margin-top: 8px;
		width: 100%;
		padding: 10px 10px;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}
</style>
<body>
	<center>
	<form method="post" enctype="multipart/form-data">
		<input  type="submit" value="Examinee Login" class="button" disabled>
		<table table id="sql">
			<tr>
				<td>
					<label for="username">Email:</label>
				</td>
				<td style="width: 77%;">
					<input type="email" id="username" name="username" required/><br><br>
				</td>
			</tr>
		</table>
		<input  type="submit" name="submit" value="Submit" class="button">
	</form>
	</center>
</body>
</html>


<?php
	if(isset($_POST['submit']))  
    { 
        $clientUsername = $_POST['username'];
		session_start();
		$arrayCounter=array();
		$_SESSION['loguser']=$clientUsername;
		$counter=rand(1,10);
		array_push($arrayCounter, $counter);
		$_SESSION['counter']=$counter;
		$_SESSION['score']=0;
		$_SESSION['questionCounter']=0;
		$_SESSION['arrayCounter']=$arrayCounter;
		header("Location: 5-1.php");
    }
?>