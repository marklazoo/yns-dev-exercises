<?php
	session_start();

	if(!isset($_SESSION['loguser']))
    {
    	header('Location: 1-13_sql.php');
    }

	$user_check = $_SESSION['loguser'];
	$login_session = '';

	$con=mysqli_connect("localhost", "root","");
	$database=mysqli_select_db($con,"exercisesSQL");

	$que = mysqli_query($con,"select * from user_info where email='$user_check'");
	$row=mysqli_fetch_assoc($que);
	$login_session = $row['email'];

	if(!isset($login_session))
	{
	    header('Location: 1-13_sql.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>SQL 3-5</title>
	<style type="text/css">
	#sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		cursor: pointer;
		width: 25%;
	}
</style>
</head>
<body>
	<center>
	<form method="post" enctype="multipart/form-data">  
		<input  type="submit" name="LogOut" value="Log Out" class="button">
	</form>
	<br>
	</center>
</body>
</html>

<?php

	$firstName = $_POST['firstName'];
    $middleName = $_POST['middleName'];
    $lastName = $_POST['lastName'];
    $gender = $_POST['gender'];
    $birthday = $_POST['birthday'];
    $email = $_POST['email'];
    $password = $_POST['password'];

	$target_dir = "user_images_sql/";
	$target_file = $target_dir.basename($_FILES["imageUpload"]["name"]);
	$uploadSuccess = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	if(isset($_POST["submit"])) 
	{
		$check = getimagesize($_FILES["imageUpload"]["tmp_name"]);
		if($check !== false) 
		{
			$uploadSuccess = 1;
		}
		else 
		{
			echo "File is not an image.";
			$uploadSuccess = 0;
		}
	}

	if (file_exists($target_file)) 
	{
		echo "Sorry, file already exists.";
		$uploadSuccess = 0;
	}

	if ($_FILES["imageUpload"]["size"] > 500000) 
	{
		echo "Sorry, your file is too large.";
		$uploadSuccess = 0;
	}

	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") 
	{
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadSuccess = 0;
	}

    $uniqueEmail = mysqli_query($con, "select * from user_info where email='$email'");
    $result = mysqli_num_rows($uniqueEmail);

    if($result == 1)
    {
    	$upload;Success == 0;
    	echo "<script type='text/javascript'>alert('Something went wrong: Email already exists!'); 
    			document.location='1-6-7_sql.php';
    			</script>";
    }

	if ($uploadSuccess == 0) 
	{
		echo "Sorry, your file was not uploaded.";

	}
	else
	{
		if (move_uploaded_file($_FILES["imageUpload"]["tmp_name"], $target_file))
		{
			echo "<center>";
			echo "<img src='".$target_file."' width='339' height='188'><br>";
			echo "<input  type='submit' value='Entered User Information' class='button' disabled>";
			echo "<table id='sql'>";
			echo "<tr><td>First Name:</td><td>".$firstName."</td></tr>";
			echo "<tr><td>Middle Name:</td><td>".$middleName."</td></tr>";
			echo "<tr><td>Last Name:</td><td>".$lastName."</td></tr>";
			echo "<tr><td>Gender:</td><td>".$gender."</td></tr>";
			echo "<tr><td>E-mail:</td><td>".$birthday."</td></tr>";
			echo "<tr><td>Birthdate:</td><td>".$email."</td></tr>";
		    echo "</table>";
		    echo "</center>";

		    $query="insert into user_info(firstName, middleName, lastName, gender, birthdate, email, password, imageName) values ('$firstName','$middleName','$lastName','$gender', '$birthday','$email','$password','$target_file')";
        	$runQuery=mysqli_query($con,$query);
		} 
		else
		{
			echo "Sorry, there was an error uploading your file.";
		}
	}
?>

<?php
	if(isset($_POST['LogOut']))  
    { 
       session_destroy();
       header('Location: 1-13_sql.php');
    }
?>