<?php
    session_start();

    if(!isset($_SESSION['loguser']))
    {
        header('Location: 1-13_sql.php');
    }

    $user_check = $_SESSION['loguser'];
    $login_session = '';

    $con=mysqli_connect("localhost", "root","");
    $database=mysqli_select_db($con,"exercisesSQL");

    $que = mysqli_query($con,"select * from user_info where email='$user_check'");
    $row=mysqli_fetch_assoc($que);
    $login_session = $row['email'];

    if(!isset($login_session))
    {
        header('Location: 1-13_sql.php');
    }

    $limit = 10;
    $count = 0;
    $counter = 0;
    $offset = $limit;

    if (isset($_GET['page']))
    {
        $counter = ($_GET['page'] - 1)*$limit;
        $offset = $limit * $_GET['page'];
    }

    $queDisplay = mysqli_query($con,"select * from user_info");
    $quePage = mysqli_num_rows($queDisplay);

?>

<!DOCTYPE html>
<html>
<head>
    <style>
        #sql 
        {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #sql td, #sql th 
        {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #sql tr:nth-child(even){background-color: #f2f2f2;}

        #sql tr:hover {background-color: #ddd;}

        #sql th 
        {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #1a8cff;
          color: white;
        }

        #button 
        {
          background-color: #1a8cff;
          border: none;
          color: white;
          padding: 15px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px 20px 20px;
          cursor: pointer;
          float: right;
        }

        .pagination 
        {
            display: inline-block;
        }

        .pagination a 
        {
          color: black;
          float: left;
          padding: 8px 16px;
          text-decoration: none;
        }

        .pagination a:hover:not(.active) 
        {
            background-color: #ddd;}
        }

    </style>
</head>
<body>
    <form method="post" enctype="multipart/form-data">  
        <input  id="button" type="submit" name="LogOut" value="Log Out">
    </form>
    <table id="sql">
        <tr align=center>
            <th> <b>Profile Picture</b> </th>
            <th> <b>First Name</b> </th>
            <th> <b>Middle Name</b> </th>
            <th> <b>Last Name</b> </th>
            <th> <b>Gender</b> </th>
            <th> <b>Birthdate</b> </th>
            <th> <b>Email</b> </th>
        </tr>
        <?php
            while($rowDisplay=mysqli_fetch_assoc($queDisplay))
            {
                $count++;
                if($count <= $counter || $count > $offset)
                {
                    continue;
                }
        ?>
            <tr align=center>
                <td> <img src="<?php echo $rowDisplay['imageName']; ?>" width="169.625" height="94.375"> </td>
                <td> <?php echo $rowDisplay['firstName']; ?> </td>
                <td> <?php echo $rowDisplay['middleName']; ?> </td>
                <td> <?php echo $rowDisplay['lastName']; ?> </td>
                <td> <?php echo $rowDisplay['gender']; ?> </td>
                <td> <?php echo $rowDisplay['birthdate']; ?> </td>
                <td> <?php echo $rowDisplay['email']; ?> </td>
                
            </tr>
        <?php
            }
        ?>
    </table><br>


    <div class="pagination">
        <?php 
            $pages = round($quePage / 10);    
            for ($i=1; $i <= $pages ; $i++)
            {
        ?>

        <a href="1-9_sql.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
        <?php 
            } 
        ?>
    </div>
</body>
</html>

<?php
    if(isset($_POST['LogOut']))  
    { 
       session_destroy();
       echo "<meta http-equiv='refresh' content='0'>";
    }
?>