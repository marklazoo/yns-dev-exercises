<!DOCTYPE html>
<html>
<head>
    <title>SQL 3-4</title>
    <style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 75%;
        font-size: 20px;
    }

    #sql tr:nth-child(even){background-color: #f2f2f2;}

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #1a8cff;
        color: white;
    }
</style>
</head>
<body>
<center>
    <br><h2>SQL 3-4: Solve problems using SQL</h2><br>
<?php 
    $con = mysqli_connect("localhost", "root","") or die ("Cannot connect!");
    $database = mysqli_select_db($con, "yns_training") or die('Cannot Access Database');

    $que1 = mysqli_query($con,"select * from employees where last_name LIKE 'K%'");
    $que2 = mysqli_query($con,"select * from employees where last_name LIKE '%i'");
    $que3 = mysqli_query($con,"select * from employees where hire_date BETWEEN'2015-01-01' AND '2015-03-31' ORDER BY hire_date ASC");
    $que4 = mysqli_query($con,"select * from employees where boss_id!='null'");
    $que5 = mysqli_query($con,"select last_name from employees where department_id=(select id from departments where name='Sales') ORDER BY last_name DESC");
    $que6 = mysqli_query($con,"select COUNT(middle_name) as count_has_middle from employees where middle_name!='null' AND middle_name!=''");
    $que7 = mysqli_query($con,"select departments.name as names, COUNT(departments.id) as totals from departments LEFT JOIN employees ON departments.id = employees.department_id where employees.id!='null' GROUP BY employees.department_id");
    $que8 = mysqli_query($con,"select *, MAX(hire_date) as latest from employees");
    $que9 = mysqli_query($con,"select departments.name as names, departments.id as ids from departments LEFT JOIN employees ON departments.id = employees.department_id where employees.id IS NULL");
    $que10 = mysqli_query($con,"select employee_id, COUNT(position_id) as counter from employee_positions GROUP BY employee_id");
			

?>
    <h3>Retrieve employees whose last name start with "K".</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>id</b> </th>
    		<th> <b>first_name</b> </th>
    		<th> <b>last_name</b> </th>
    		<th> <b>middle_name</b> </th>
    		<th> <b>department_id</b> </th>
    		<th> <b>hire_date</b> </th>
    		<th> <b>boss_id</b> </th>
    	</tr>
    	<?php
    		while($row1=mysqli_fetch_assoc($que1))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row1['id']; ?> </td>
    			<td> <?php echo $row1['first_name']; ?> </td>
    			<td> <?php echo $row1['last_name']; ?> </td>
    			<td> <?php echo $row1['middle_name']; ?> </td>
    			<td> <?php echo $row1['department_id']; ?> </td>
    			<td> <?php echo $row1['hire_date']; ?> </td>
    			<td> <?php echo $row1['boss_id']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve employees whose last name end with "i".</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>id</b> </th>
    		<th> <b>first_name</b> </th>
    		<th> <b>last_name</b> </th>
    		<th> <b>middle_name</b> </th>
    		<th> <b>department_id</b> </th>
    		<th> <b>hire_date</b> </th>
    		<th> <b>boss_id</b> </th>
    	</tr>
    	<?php
    		while($row2=mysqli_fetch_assoc($que2))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row2['id']; ?> </td>
    			<td> <?php echo $row2['first_name']; ?> </td>
    			<td> <?php echo $row2['last_name']; ?> </td>
    			<td> <?php echo $row2['middle_name']; ?> </td>
    			<td> <?php echo $row2['department_id']; ?> </td>
    			<td> <?php echo $row2['hire_date']; ?> </td>
    			<td> <?php echo $row2['boss_id']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date. (Hint. Use CONCAT)</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>Full Name</b> </th>
    		<th> <b>hire_date</b> </th>
    	</tr>
    	<?php
    		while($row3=mysqli_fetch_assoc($que3))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row3['first_name'].' '.$row3['middle_name'].' '.$row3['last_name']; ?> </td>
    			<td> <?php echo $row3['hire_date']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>Employee</b> </th>
    		<th> <b>Boss</b> </th>
    	</tr>
    	<?php
    		while($row4=mysqli_fetch_assoc($que4))
    		{
    			$var = $row4['boss_id'];
    			$que4continue = mysqli_query($con,"select * from employees where id='$var'");
    			$row4continue = mysqli_fetch_assoc($que4continue);
    	?>
    		<tr align=center>
    			<td> <?php echo $row4['last_name']; ?> </td>
    			<td> <?php echo $row4continue['last_name'];?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve employee's last name who belong to Sales department ordered by descending by last name.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>last_name</b> </th>
    	</tr>
    	<?php
    		while($row5=mysqli_fetch_assoc($que5))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row5['last_name']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve number of employee who has middle name.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>count_has_middle</b> </th>
    	</tr>
    	<?php
    		while($row6=mysqli_fetch_assoc($que6))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row6['count_has_middle']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>
 
	<h3>Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>Full Name</b> </th>
    		<th> <b>hire_date</b> </th>
    	</tr>
    	<?php
    		while($row7=mysqli_fetch_assoc($que7))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row7['names']; ?> </td>
    			<td> <?php echo $row7['totals']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve employee's full name and hire date who was hired the most recently.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>first_name</b> </th>
    		<th> <b>middle_name</b> </th>
    		<th> <b>last_name</b> </th>
    		<th> <b>hire_date</b> </th>
    	</tr>
    	<?php
    		while($row8=mysqli_fetch_assoc($que8))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row8['first_name']; ?> </td>
    			<td> <?php echo $row8['middle_name']; ?> </td>
    			<td> <?php echo $row8['last_name']; ?> </td>
    			<td> <?php echo $row8['hire_date']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>

    <h3>Retrieve department name which has no employee.</h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>name</b> </th>
    		<th> <b>id</b> </th>
    	</tr>
    	<?php
    		while($row9=mysqli_fetch_assoc($que9))
    		{
    	?>
    		<tr align=center>
    			<td> <?php echo $row9['names']; ?> </td>
    			<td> <?php echo $row9['ids']; ?> </td>
    		</tr>
    	<?php
    		}
    	?>
    </table><br>  

    <h3>Retrieve employee's full name who has more than 2 positions </h3>
    <table id="sql">
    	<tr align=center>
    		<th> <b>first_name</b> </th>
    		<th> <b>middle_name</b> </th>
    		<th> <b>last_name</b> </th>
    	
    	</tr>
    	<?php
    		while($row10=mysqli_fetch_assoc($que10))
    		{
    			if($row10['counter']>1)
    			{
    				$counter = $row10['employee_id'];
    				$que10continue = mysqli_query($con,"select * from employees where id=$counter");
    				$row10continue = mysqli_fetch_assoc($que10continue);
    	?>
		    		<tr align=center>
		    			<td> <?php echo $row10continue['first_name']; ?> </td>
		    			<td> <?php echo $row10continue['middle_name']; ?> </td>
		    			<td> <?php echo $row10continue['last_name']; ?> </td>
		    		</tr>
    	<?php
    			}
    		}
    	?>
    </table><br>
</center>			
</body>
</html>