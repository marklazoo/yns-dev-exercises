<?php
	$con=mysqli_connect("localhost", "root","");
    $database=mysqli_select_db($con,"exercisesSQL");
    $que = mysqli_query($con,"select * from examples");
    $count = mysqli_num_rows($que);

?>

<!DOCTYPE html>
<head>
<title>SQL 3-3</title>
<style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
        background-color: #1a8cff;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        margin: 10px 10px;
        cursor: pointer;
        width: 25%;
    }

    input[type=text], select {
        width: 100%;
        padding: 10px 10px;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>
</head>
<html>
<body>
    <center>
    <br>
    <h3>SQL 3-3: Insert, Update, Delete</h3>
	<form method="post" enctype="multipart/form-data">
        <table id="sql">
            <tr>
                <td>
                    <label for="exNames">Exanple Name:</label>
                </td>
                <td>
                    <input type="text" id="exNames" name="exNames"  pattern="[A-Za-z ]+" title="Letters Only" required placeholder="enter name"/>
                </td>
            </tr>
            <tr>
                <td>
                   <label for="exAbouts">Example About:</label>  
                </td>
                <td>
                    <input type="text" id="exAbouts" name="exAbouts" pattern="[A-Za-z ]+" title="Letters Only" required placeholder="enter about"/>
                </td>
            </tr>
        </table>
		<input  type="submit" name="submit" value="Insert" class="button">
	</form>
    <br>
	<form method="post" enctype="multipart/form-data">
        <table id="sql">
            <tr>
                <td>
                    <label for="toDelete">ID to be deleted:</label> 
                </td>
                <td>
                    <input type="text" id="toDelete" name="toDelete" title="Letters Only" required placeholder="enter id"/>
                </td>
            </tr>
        </table>
		<input  type="submit" name="delete" value="Delete" class="button">
	</form> 
    <br>
	<form method="post" enctype="multipart/form-data">
        <table id="sql">
            <tr>
                <td>
                   <label for="toUpdate">ID to be updated:</label>   
                </td>
                <td>
                    <input type="text" id="toUpdate" name="toUpdate" title="Letters Only" required placeholder="enter id"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="exNamesUp">Update Name to:</label> 
                </td>
                <td>
                    <input type="text" id="exNamesUp" name="exNamesUp"  pattern="[A-Za-z ]+" title="Letters Only" required placeholder="enter name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="exAboutsUp">Update example to:</label> 
                </td>
                <td>
                    <input type="text" id="exAboutsUp" name="exAboutsUp" pattern="[A-Za-z ]+" title="Letters Only" required placeholder="enter about"/>
                </td>
            </tr>
        </table>
		<input  type="submit" name="update" value="Update" class="button">
	</form>

	<?php
		if($count == 0)
		{
			echo "<h4>No Available Data to Display</h4>";
		}
		else
		{
	?>
	<br>
	<table id="sql">
        <tr align=center>
            <th> <b>ID</b> </th>
            <th> <b>Name</b> </th>
            <th> <b>About</b> </th>
        </tr>
        <?php
            while($rowDisplay=mysqli_fetch_assoc($que))
            {
        ?>
            <tr align=center>
                <td> <?php echo $rowDisplay['exID']; ?> </td>
                <td> <?php echo $rowDisplay['exName']; ?> </td>
                <td> <?php echo $rowDisplay['exAbout']; ?> </td>
                
            </tr>
        <?php
            }
        ?>
    </table><br>
    <?php
    	}
    ?>
    </center>
</body>
</html>

<?php
	if(isset($_POST['submit']))  
    { 
    	$name = $_POST['exNames'];
    	$about = $_POST['exAbouts'];
    	$que = mysqli_query($con,"insert into examples(exName, exAbout) values ('$name', '$about')");
        echo "<meta http-equiv='refresh' content='0'>";
    }

    if(isset($_POST['delete']))  
    { 
    	$id = $_POST['toDelete'];
    	$queDelete = mysqli_query($con,"select * from examples where exID='$id'");
    	$countDel = mysqli_num_rows($queDelete);
    	if($countDel == 0)
    	{
    		echo "<script type='text/javascript'>alert('ID does not exist!');</script>";
    	}
    	else
    	{
    		$que = mysqli_query($con,"delete from examples where exID='$id'");
    		echo "<script type='text/javascript'>alert('Record deleted!');</script>";
    	}
        echo "<meta http-equiv='refresh' content='0'>";
    }

    if(isset($_POST['update']))  
    { 
    	$id = $_POST['toUpdate'];
    	$name = $_POST['exNamesUp'];
    	$about = $_POST['exAboutsUp'];
    	$queUpdate = mysqli_query($con,"select * from examples where exID='$id'");
    	$countUpdate = mysqli_num_rows($queUpdate);
    	if($countUpdate == 0)
    	{
    		echo "<script type='text/javascript'>alert('ID does not exist!');</script>";
    	}
    	else
    	{
    		$que = mysqli_query($con,"update examples set exName='$name', exAbout='$about' where exID='$id'");
    		echo "<script type='text/javascript'>alert('Record updated!');</script>";
    	}
        echo "<meta http-equiv='refresh' content='0'>";
    }
?>