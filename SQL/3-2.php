<!DOCTYPE html>
<html>
<title>SQL 3-2</title>
<head>
	<style type="text/css">
	#sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		margin: 10px 10px;
		cursor: pointer;
		width: 25%;
	}

	input[type=text], select {
		width: 100%;
		padding: 10px 10px;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}
</style>
</head>
<body>
	<center>
	<h3>SQL 3-2: Create Table</h3>
	<form method="post" enctype="multipart/form-data">
		<input  type="submit" value="PRESS SUBMIT TO CREATE TABLE" class="button" disabled>
		<table id="sql">
			<tr>
				<td>
					<label for="tableName">table name </label>
				</td>
				<td>
					<input type="text" id="tableName" name="tableName" required placeholder="enter table name"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="primaryKey">int(11) </label>
				</td>
				<td>
					<input type="text" id="primaryKey" name="primaryKey" required placeholder="enter primary key"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="field2">varchar(255) </label>
				</td>
				<td>
					<input type="text" id="field2" name="field2"  required placeholder="enter column name"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="field3">varchar(255) </label>
				</td>
				<td>
					<input type="text" id="field3" name="field3" required placeholder="enter column name"/>
				</td>
			</tr>
		</table>
		<input  type="submit" name="submit" value="Submit" class="button">
	</form>
	</center>
</body>
</html>

<?php
	if(isset($_POST['submit']))  
    { 
    	$table = $_POST['tableName'];
    	$primaryKey = $_POST['primaryKey'];
    	$field2 = $_POST['field2'];
    	$field3 = $_POST['field3'];

        $con=mysqli_connect("localhost", "root","");
	    $database=mysqli_select_db($con,"exercisessql");

	    $query = "create table $table ($primaryKey int(11), $field2 varchar(255), $field3 varchar(255), PRIMARY KEY ($primaryKey))";
	    $result=mysqli_query($con, $query) or die ("A problem occured. Table already exists.");
	    if($result)
	    {
	    	echo "<script type='text/javascript'>alert('Table successfully created!');</script>";
	    }
	    else
	    {
	    	echo "A problem occured. Table not Created.";
	    }
    }
?>