<!DOCTYPE html>
<html>

<head>
    <style type="text/css">
        .dateToday {
            background-color:#99ccff!important
        }
        .custom {
            padding: 5% 5% 5% 30%;
            margin-bottom:0;
            background-color:rgba(0,0,0,.03);
            border-bottom:1px solid rgba(0,0,0,.125);
        }
    </style>
    <meta charset="UTF-8">
    <title>Practices 5-2</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

</head>
<body>
    <br>
<center><h3>Practices 5-2: Calendar</h3></center>
<div class="container col-sm-4 col-md-7 col-lg-4 mt-5">
    <div class="card">
        <div class="form-inline custom">
                <button class="btn btn-outline-primary" id="previous" onclick="prev()">&#8249;</button>
                &nbsp;&nbsp;<h3  id="monthYear"></h3>&nbsp;&nbsp;
                <button class="btn btn-outline-primary" id="next" onclick="next()">&#8250;</button>
        </div>
        
        <table class="table table-bordered table-responsive-sm" id="calendar">
            <thead>
            <tr>
                <th>Sun</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
            </tr>
            </thead>

            <tbody id="calendarContent">

            </tbody>
        </table>

        
        <br/>
    </div>
</div>

<script>
    let today = new Date();
    let curMonth = today.getMonth();
    let curYear = today.getFullYear();

    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    let monthYear = document.getElementById("monthYear");
    showCalendar(curMonth, curYear);

    function next() {
        curYear = (curMonth === 11) ? curYear + 1 : curYear;
        curMonth = (curMonth + 1) % 12;
        showCalendar(curMonth, curYear);
    }

    function prev() {
        curYear = (curMonth === 0) ? curYear - 1 : curYear;
        curMonth = (curMonth === 0) ? 11 : curMonth - 1;
        showCalendar(curMonth, curYear);
    }

    function showCalendar(month, year) {

        let firstDay = (new Date(year, month)).getDay();
        let daysInMonth = 32 - new Date(year, month, 32).getDate();

        let tbl = document.getElementById("calendarContent"); 

        tbl.innerHTML = "";

        monthYear.innerHTML = months[month] + " " + year;

        let date = 1;
        for (let i = 0; i < 6; i++) {

            let row = document.createElement("tr");

            for (let j = 0; j < 7; j++) {
                if (i === 0 && j < firstDay) {
                    let cell = document.createElement("td");
                    let cellText = document.createTextNode("");
                    cell.appendChild(cellText);
                    row.appendChild(cell);
                }
                else if (date > daysInMonth) {
                    break;
                }
                else {
                    let cell = document.createElement("td");
                    let cellText = document.createTextNode(date);
                    if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
                        cell.classList.add("dateToday");
                    }
                    cell.appendChild(cellText);
                    row.appendChild(cell);
                    date++;
                }


            }

            tbl.appendChild(row);
        }

    }
</script>

</body>
</html>