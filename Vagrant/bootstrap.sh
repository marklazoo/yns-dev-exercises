#!/usr/bin/env bash

yum update -y
yum -y install httpd

echo "Using bootstrap.sh: Search and Get EPEL Repository on CentOS 7."
yum search epel-release
yum info epel-release
yum install epel-release

# PHP Mods
yum install -y php7.2-common
yum install -y php7.2-mcrypt
yum install -y php7.2-zip

yum install -y yum-utils
yum install -y php
yum install -y mysql-server

yum -y install phpmyadmin

echo "Using bootstrap.sh: Starting the HTTP(PHPMyadmin)."
service httpd start
service mysqld start

echo "Using bootstrap.sh: Creating the HTML directory."
mkdir html
echo "Initial setting up of provisions is done."