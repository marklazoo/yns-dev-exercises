<html>
<head>
    <title>HTML & PHP 1-2</title>
    <style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
        background-color: #1a8cff;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        cursor: pointer;
        width: 25%;
    }

    .buttonOperation {
        background-color: #1a8cff;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        cursor: pointer;
        width: 100%;
    }

    input[type=number], select {
        width: 100%;
        padding: 10px 10px;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>
</head>  
<body> 
<center>
<h3>HTML & PHP 1-2: The four basic operations of arithmetic.</h3>
<form method="post">
    <input  type="submit" value="Arithmetic Operations" class="button" disabled>
    <table id="sql">
        <tr>
            <td>
                <label for="val1">First Number:</label>
            </td>
            <td>
                <input type="number" id="val1" name="val1" required> 
            </td>
        </tr>
        <tr>
            <td>
                <label for="val2">Second Number:</label>
            </td>
            <td>
                <input type="number" id="val2" name="val2" required>
            </td>
        </tr>
        <tr>
            <td>
                <input  type="submit" name="submit" value="Add" class="buttonOperation">
            </td>
            <td>
                <input  type="submit" name="submit" value="Subtract" class="buttonOperation">
            </td>
        </tr>
        <tr>
            <td>
                <input  type="submit" name="submit" value="Multiply" class="buttonOperation">
            </td>
            <td>
                <input  type="submit" name="submit" value="Divide" class="buttonOperation">
            </td>
        </tr>
    </table>	
</form>
</center>    
</body>  
</html> 

<?php  
    if(isset($_POST['submit']))  
    { 
        $operation = $_POST['submit'];
        $number1 = $_POST['val1'];  
        $number2 = $_POST['val2'];  
        if($operation=="Add")
        {
            $sum =  $number1+$number2;
            echo "<center><h3>Sum is ".$sum."</h3></center>";
        }
        else if($operation=="Subtract")
        {
            $difference =  $number1-$number2;
            echo "<center><h3>Difference is ".$difference."</h3></center>";
        }
        else if($operation=="Multiply")
        {
            $product =  $number1*$number2;
            echo "<center><h3>Product is ".$product."</h3></center>";
        }
        else if($operation=="Divide")
        {
            $quotient =  $number1/$number2;
            echo "<center><h3>Quotient is ".$quotient."</h3></center>";
        }
    }  
?>