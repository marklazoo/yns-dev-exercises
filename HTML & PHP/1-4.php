<html>
<head>
    <title>HTML & PHP 1-4</title>
    <style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
        background-color: #1a8cff;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        cursor: pointer;
        width: 25%;
    }

    input[type=number], select {
        width: 100%;
        padding: 10px 10px;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>
</head>   
<body>
<center>
<h3>HTML & PHP 1-4: Solve FizzBuzz problem.</h3>
<form method="post">
    <input  type="submit" value="FizzBuzz Calculator" class="button" disabled>
    <table id="sql">
        <tr>
            <td>
                <label for="val1">First Number:</label>
            </td>
            <td>
                <input type="number" id="val1" name="val1" required> 
            </td>
        </tr>
    </table>
    <input  type="submit" name="submit" value="Calculate" class="button">	
</form>
</center> 
</body>  
</html> 

<?php  
    if(isset($_POST['submit']))  
    { 
        $number1 = $_POST['val1'];
        echo "<center><h3>";
        for ($i = 1; $i <= $number1; $i++) 
        {
		    if ($i % 15 == 0) {
		        echo 'FizzBuzz<br>';
		    } elseif ($i % 3 == 0) {
		        echo 'Fizz<br>';
		    } elseif ($i % 5 == 0) {
		        echo 'Buzz<br>';
		    } else {
		        echo $i . '<br>';
		    }
		}
		echo "</h3></center>";
	}  
?>