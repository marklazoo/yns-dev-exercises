<?php

    session_start();

    if(!isset($_SESSION['loguser']))
    {
       header('Location: 1-13.php');
    }

    $user_check=$_SESSION['loguser'];

    $login_session = '';

    $file = "user_info.csv";
    $clients = file($file, FILE_IGNORE_NEW_LINES);
        
    foreach ($clients as $index => $client_line) 
    {
        $split = str_getcsv($client_line);
        if ($split[6] == $user_check) {
            $login_session = $split[6];
        }
    }

    if(!isset($login_session))
    {
        header('Location: 1-13.php');
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>HTML & PHP 1-6 to 1-13</title>
    <style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: center;
    }

    #sql tr:nth-child(even){background-color: #f2f2f2;}

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        background-color: #1a8cff;
        color: white;
    }

    .button {
        background-color: #ff4d4d;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        cursor: pointer;
        width: 100%;
        margin-bottom: 10px;
    }
</style>
</head>
<body>
    <form method="post" enctype="multipart/form-data">  
        <input  class="button" type="submit" name="LogOut" value="Log Out">
    </form>


    <?php

        $limit = 10;
        $count = 0;
        $counter = 0;
        $offset = $limit;

            if (isset($_GET['page'])) {
                $counter = ($_GET['page'] - 1)*$limit;
                $offset = $limit * $_GET['page'];
            }

            echo "<center><table id='sql'>";
            echo "<tr><th>ID</th><th>First Name</th><th>Middle Name</th><th>Last Name</th><th>Gender</th><th>Birthday</th><th>Email</th><th>Image</th></tr>";

            $file = fopen("user_info.csv", "r");
            while (($data = fgetcsv($file)) !== false) 
            {
                $count++;
                if($count <= $counter || $count > $offset)
                {
                    continue;
                }

                echo "<tr>";

                foreach ($data as $key => $i) 
                {
                    if(strpos($i, "user_images") !== false)
                    {
                        echo "<td><img src='".$i."' width='339' height='188'></td>";
                    }
                    else
                    {
                        if($key!==7){
                            echo "<td>" . htmlspecialchars($i). "</td>";
                        }
                    }  
                }
                echo "</tr> \n";
            }
          
            fclose($file);
            echo "</table></center>";
        ?>

    <ul>
        <?php 
            $fp = file('user_info.csv');
            $pages = round(count($fp) / 10);    
            for ($i=1; $i <= $pages ; $i++)
            {
        ?>

        <li><a href="1-9.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php 
            } 
        ?>
    </ul>
</body>
</html>

<?php
    if(isset($_POST['LogOut']))  
    { 
       session_destroy();
       header('Location: 1-13.php');
    }
?>