<?php
	session_start();
	session_destroy();
?> 

<!DOCTYPE html>
<html>
<head>
	<title>Javascript 1-6 to 1-13</title>
	<style type="text/css">
	#sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 25%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		margin: 10px 10px;
		cursor: pointer;
		width: 25%;
	}

	input[type=email], input[type=password], select {
		width: 100%;
		padding: 10px 10px;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}
</style>
</head>
<body>
	<center>
	<form method="post" enctype="multipart/form-data">
		<input  type="submit" value="User Login" class="button" disabled>
		<table id="sql">
			<tr>
				<td>
					<label for="username">Email:</label> 
				</td>
				<td>
					<input type="email" id="username" name="username" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="password">Password:</label>  
				</td>
				<td>
					<input type="password" id="password" name="password" required/>
				</td>
			</tr>
		</table>
		<input  type="submit" name="submit" value="Submit" class="button">
	</form>
	</center>
</body>
</html>


<?php
	if(isset($_POST['submit']))  
    { 
        $clientUsername = $_POST['username'];
		$clientPassword = $_POST['password'];
		$file = "user_info.csv";
		$clients = file($file, FILE_IGNORE_NEW_LINES);
		
		foreach ($clients as $index => $client_line) 
		{
		    $split = str_getcsv($client_line);
		    if ($split[6] == $clientUsername) 
		    {
		    	echo "gggggg";
		        if (isset($split[7]) && $split[7] == $clientPassword) 
		        {
		            unset($clients[$index]);
		            session_start();

					$_SESSION['loguser']=$clientUsername;
					$_SESSION['logpass']=$clientPassword;
					header("Location: 1-6-7.php");
		        }
		        break;
		    }
		    else
		    {
		    	header("Location: 1-13.php");
		    }
		}
    }
?>
