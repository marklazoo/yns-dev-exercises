<?php

	session_start();

	if(!isset($_SESSION['loguser']))
    {
       header('Location: 1-13.php');
    }

	$user_check = $_SESSION['loguser'];
	$login_session = '';

	$file = "user_info.csv";
	$clients = file($file, FILE_IGNORE_NEW_LINES);
		
	foreach ($clients as $index => $client_line) 
	{
		$split = str_getcsv($client_line);
		
		if ($split[6] == $user_check) 
		{
		    $login_session = $split[6];
		}
	}

	if(!isset($login_session))
	{
	    header('Location: 1-13.php');
	}
?>

<html>
<head>
	<title>SQL 3-5</title>
	<style type="text/css">
	#sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 40%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		cursor: pointer;
		width: 40%;
	}

	.buttonExtra {
		background-color: #66c2ff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		cursor: pointer;
		width: 19.8%;
	}

	input[type=email], input[type=password], input[type=text], input[type=date], input[type=file], select {
		width: 100%;
		padding: 10px 10px;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}
</style>
</head> 
<body>
	<center>
	<br>
	<form method="post" enctype="multipart/form-data">  
		<input  type="submit" name="LogOut" value="Log Out" class="buttonExtra">
		<input  type="submit" name="listPage" value="View List" class="buttonExtra">
	</form> 
	<form method="post" action=1-8.php enctype="multipart/form-data">
		<input  type="submit" value="Basic User Information:" class="button" disabled>
		<table id="sql">
			<tr>
				<td>
					<label for="imageUpload">Image:</label>
				</td>
				<td>
					<input type="file" name="imageUpload" id="imageUpload" required>
				</td>
			</tr>
			<tr>
				<td>
					<label for="firstName">First Name:</label>
				</td>
				<td>
					<input type="text" id="firstName" name="firstName" pattern="[A-Za-z ]+" title="Letters Only" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="middleName">Middle Name:</label>
				</td>
				<td>
					<input type="text" id="middleName" name="middleName"  pattern="[A-Za-z ]+" title="Letters Only" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="lastName">Last Name:</label> 
				</td>
				<td>
					<input type="text" id="lastName" name="lastName" pattern="[A-Za-z ]+" title="Letters Only" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="gender">Gender:</label>
				</td>
				<td>
					<input type="text" id="gender" name="gender" pattern="[A-Za-z]+" title="Letters Only" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="birthday">Birthday:</label>
				</td>
				<td>
					<input type="date" id="birthday" name="birthday" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="email">E-mail:</label>
				</td>
				<td>
					<input type="email" id="email" name="email" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="password">Password:</label>  
				</td>
				<td>
					<input type="password" id="password" name="password" required/>
				</td>
			</tr>
		</table>
		<input  type="submit" name="submit" value="Submit" class="button">
	</form>
	</center>
</body> 
</html>

<?php
	if(isset($_POST['LogOut']))  
    { 
       session_destroy();
       header('Location: 1-13.php');
    }
    if(isset($_POST['listPage']))  
    { 
    	$_SESSION['loguser']=$login_session;
        header('Location: 1-9.php?page=1');
    }
?> 

