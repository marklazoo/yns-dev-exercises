<html>
<head>
    <title>HTML & PHP 1-5</title>
    <style type="text/css">
    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 30%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:hover {
        background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
        background-color: #1a8cff;
        border: none;
        color: white;
        padding: 10px 15px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        cursor: pointer;
        width: 30%;
    }

    input[type=text], select {
        width: 100%;
        padding: 10px 10px;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>
</head>   
<body>
<center>
<h3>HTML & PHP 1-5: Input date. Then show 3 days from inputted date and its day of the week.</h3>
<form method="post">
    <input  type="submit" value="Date Calculator" class="button" disabled>
    <table id="sql">
        <tr>
            <td>
                <label for="date">Enter Date:</label>
            </td>
            <td>
                <input type="text" id="date" name="date" placeholder="YYYY-MM-DD" required>
            </td>
        </tr>
    </table>
    <input  type="submit" name="submit" value="Submit" class="button">   
</form>
</center> 
</body>  
</html> 

<?php  
    if(isset($_POST['submit']))  
    { 
        echo "<center><h3>";
        $date = date('Y-m-d', strtotime($_POST['date']. ' + 3 days'));
        $unixTimestamp = strtotime($date);
        $dayOfWeek = date("l", $unixTimestamp);
        echo $date . ' is a ' . $dayOfWeek;
        echo "</h3></center>";
	}  
?>