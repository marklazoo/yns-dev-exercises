<!DOCTYPE html>
<html>
<head>
  <title>Practices 5-3</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    body {
      font-family: "Lato", sans-serif;
    }

    .sidenav {
      height: 100%;
      width: 20%;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      padding-top: 20px;
    }

    .sidenav a, .dropdown-btn {
      padding: 6px 36px 6px 36px;
      text-decoration: none;
      font-size: 20px;
      color: #818181;
      display: block;
      border: none;
      background: none;
      width: 100%;
      text-align: left;
      cursor: pointer;
      outline: none;
    }

    .sidenav a:hover, .dropdown-btn:hover {
      color: #f1f1f1;
    }

    .main {
      margin-left: 20%;
      font-size: 20px; 
      padding: 0px 10px;
    }

    .active {
      background-color: #1a8cff;
      color: white;
    }


    .dropdown-container {
      display: none;
      background-color: #262626;
      padding-left: 30px;
    }

    .fa-caret-down {
      float: right;
      padding-right: 8px;
    }
  </style>
</head>
<body>

<div class="sidenav">
  <button class="dropdown-btn">PHP Exercises 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href='HTML & PHP/1-1.php' target="_blank">1-1</a>
    <a href='HTML & PHP/1-2.php' target="_blank">1-2</a>
    <a href='HTML & PHP/1-3.php' target="_blank">1-3</a>
    <a href='HTML & PHP/1-4.php' target="_blank">1-4</a>
    <a href='HTML & PHP/1-5.php' target="_blank">1-5</a>
    <a href='HTML & PHP/1-6 - 1-13/1-13.php' target="_blank">1-6 to 1-13</a>
  </div>
  <button class="dropdown-btn">Javascript 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href='Javascript/2-1.html' target="_blank">2-1</a>
    <a href='Javascript/2-2.html' target="_blank">2-2</a>
    <a href='Javascript/2-3.html' target="_blank">2-3</a>
    <a href='Javascript/2-4.html' target="_blank">2-4</a>
    <a href='Javascript/2-5.html' target="_blank">2-5</a>
    <a href='Javascript/2-6.html' target="_blank">2-6</a>
    <a href='Javascript/2-7.html' target="_blank">2-7</a>
    <a href='Javascript/2-8.html' target="_blank">2-8</a>
    <a href='Javascript/2-9.html' target="_blank">2-9</a>
    <a href='Javascript/2-10.html' target="_blank">2-10</a>
    <a href='Javascript/2-11.html' target="_blank">2-11</a>
    <a href='Javascript/2-12.html' target="_blank">2-12</a>
    <a href='Javascript/2-13.html' target="_blank">2-13</a>
    <a href='Javascript/2-14.html' target="_blank">2-14</a>
    <a href='Javascript/2-15.html' target="_blank">2-15</a>
  </div>
  <button class="dropdown-btn">SQL
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href='SQL/3-2.php' target="_blank">3-2</a>
    <a href='SQL/3-3.php' target="_blank">3-3</a>
    <a href='SQL/3-4.php' target="_blank">3-4</a>
    <a href='SQL/3-5/1-13_sql.php' target="_blank">3-5</a>
  </div>
  <button class="dropdown-btn">Practice 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="5-1.php" target="_blank">5-1</a>
    <a href="5-2.php" target="_blank">5-2</a>
  </div>
  <button class="dropdown-btn">Advanced SQL 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href='advanced_sql/7-1.php' target="_blank">7-1</a>
    <a href='advanced_sql/7-2.php' target="_blank">7-2</a>
    <a href='advanced_sql/7-3.php' target="_blank">7-3</a>
    <a href='advanced_sql/7-4.php' target="_blank">7-4</a>
  </div>
  
</div>

<div class="main" id="clear">
  <h2>Practices 5-3: Navigation</h2>
  <h4>Browse through the side navigation bar to view YNS training exercises.</h4>
  <h4>Note: All exercises will be opened in a new tab.</h4>
</div>

<script>

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } 
    else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>

</body>
</html> 