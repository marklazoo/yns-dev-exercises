<?php
session_start();

if(!isset($_SESSION['loguser']))
{
    header('Location: 5-1_log.php');
}

$con=mysqli_connect("localhost", "root","");
$database=mysqli_select_db($con,"yns_practices");

$clientUsername = $_SESSION['loguser'];
if(isset($_SESSION['arrayCounter']))
{
    $arrayCounter = $_SESSION['arrayCounter'];
}
else
{
	$arrayCounter = $_SESSION['arrayCounter']=array();
}
if(isset($_SESSION['counter']))
{
	$_SESSION['questionData'] = array();
	$_SESSION['answerData'] = array();
	$questionDone = $_SESSION['counter'];
	$que1 = mysqli_query($con,"select * from questions where id='$questionDone'");
	$que2 = mysqli_query($con,"select * from answers where question_id='$questionDone'");
   	while($row1=mysqli_fetch_assoc($que1)){
   		array_push($_SESSION['questionData'],$row1['id']);
   		array_push($_SESSION['questionData'],$row1['question_value']);
   		array_push($_SESSION['questionData'],$row1['answer_id']);
   	}
   	while($row2=mysqli_fetch_assoc($que2)){
   		$answersCounter=array("id"=>$row2['id'],"answer_value"=>$row2['answer_value']);
   		array_push($_SESSION['answerData'],$answersCounter);
   	}
}
?>
<!DOCTYPE html>
<html>
<style type="text/css">
	.disabledbutton {
	    pointer-events: none;
	    opacity: 0.4;
	}

    #sql {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 50%;
        font-size: 20px;
    }

    #sql td, #sql th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sql tr:nth-child(even){
    	background-color: #f2f2f2;
    }

    #sql tr:hover {
    	background-color: #ddd;
    }

    #sql th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #1a8cff;
        color: white;
    }

    .button {
		background-color: #1a8cff;
		border: none;
		color: white;
		padding: 10px 15px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 20px;
		margin: 10px 10px;
		cursor: pointer;
		width: 50%;
	}

</style>
<head>
	<title></title>
</head>
<body>
	<center>
	<div>
		<form method=POST>
			<input type="submit" name="reset" id="reset" value="Reset"class="button">
		</form>
	</div>
	<div id="questionsDiv" style="display:block;">
		<form method=POST>
			<table table id="sql">
				<tr>
					<th style="width: 5%;"> Question: </th>
					<th><?php echo $_SESSION['questionData'][1];?></th>
				</tr>
				<tr>
					<td style="text-align: center;">
						<input type="radio" id="choiceA" name="answer" value="<?php echo $_SESSION['answerData'][0]['id'];?>" required>
					</td>
					<td>
						<label for="choiceA" id="letterA"><?php echo $_SESSION['answerData'][0]['answer_value'];?></label><br>
					</td>
				</tr>
				<tr>
					<td style="text-align: center;">
						<input type="radio" id="choiceB" name="answer" value="<?php echo $_SESSION['answerData'][1]['id'];?>" required>
					</td>
					<td>
						<label for="choiceB" id="letterB"><?php echo $_SESSION['answerData'][1]['answer_value'];?></label><br>
					</td>
				</tr>
				<tr>
					<td style="text-align: center;">
						<input type="radio" id="choiceC" name="answer" value="<?php echo $_SESSION['answerData'][2]['id'];?>" required>
					</td>
					<td>
						<label for="choiceC" id="letterC"><?php echo $_SESSION['answerData'][2]['answer_value'];?></label>
					</td>
				</tr>
			</table>
			
			<input type="hidden" id="correct" name="correct" value="<?php echo $_SESSION['questionData'][2];?>">
			<input type="submit" name="submit" id="submit" value="Submit" class="button">
		</form>
	</div>

	<div id="welcomeDiv" style="display:none;">
		<h2 id="input"></h2>
	</div>

	<?php
		function myRecursiveFunction($arrayCounterVar) {
			$questionID = rand(1,10);
	    	if (!in_array($questionID, $arrayCounterVar))
			{
			  	array_push($arrayCounterVar,$questionID);
	    	  	$_SESSION['arrayCounter'] = $arrayCounterVar;
	    		$_SESSION['counter'] = $questionID;
	    	  	return;
			}
			else
			{
			  myRecursiveFunction($arrayCounterVar);
			}
		}
		if(isset($_POST['reset']))  
	    {
	    	session_destroy();
       		header('Location: 5-1_log.php');
	    }
		if(isset($_POST['submit']))  
	    { 
	    	$questionCounter = $_SESSION['questionCounter'];
	    	$score = $_SESSION['score'];
	    	$questionCounter = $questionCounter + 1;
	    	if($_POST['answer'] == $_POST['correct'])
	    	{
	    		$score = $score + 1;
	    		$_SESSION['score'] = $score;

	    	}
	    	else
	    	{
	    		$_SESSION['score'] = $score;
	    	}
	    	
	    	$arrayCounterVar = $_SESSION['arrayCounter'];
	    	if(count($arrayCounterVar) != 10)
	    	{
	    		list ($questionID, $arrayCounterVar) = myRecursiveFunction($arrayCounterVar);
	    		
			}

			if($questionCounter == 10)
	    	{
	    		echo "<script type='text/javascript'>
						document.getElementById('welcomeDiv').style.display = 'block';
						document.getElementById('questionsDiv').style.display = 'none';
						document.getElementById('input').innerHTML = 'Congratulations ".$clientUsername.". You scored a total of ".$score." out of 10.';
						</script>";
	    	}
	    	else
	    	{
	    		$_SESSION['questionCounter'] = $questionCounter;
	    	}
	    	
	    }
	    else
	    {
	    	$arrayCounterVar = $_SESSION['arrayCounter'];
	    	list ($questionID, $arrayCounterVar) = myRecursiveFunction($arrayCounterVar);
	    }
	?>
</center>
</body>
</html>