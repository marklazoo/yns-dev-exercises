-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2021 at 04:04 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercisessql`
--

-- --------------------------------------------------------

--
-- Table structure for table `advancedsql`
--

CREATE TABLE `advancedsql` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advancedsql`
--

INSERT INTO `advancedsql` (`id`, `parent_id`) VALUES
(1, NULL),
(2, 5),
(3, NULL),
(4, 1),
(5, NULL),
(6, 3),
(7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_shifts`
--

CREATE TABLE `daily_work_shifts` (
  `id` int(11) NOT NULL,
  `therapist_id` int(10) NOT NULL,
  `target_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_work_shifts`
--

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES
(1, 1, '2021-04-10', '14:00:00', '15:00:00'),
(2, 2, '2021-04-10', '22:00:00', '23:00:00'),
(3, 3, '2021-04-10', '00:00:00', '01:00:00'),
(4, 4, '2021-04-10', '05:00:00', '05:30:00'),
(5, 1, '2021-04-10', '21:00:00', '21:45:00'),
(6, 5, '2021-04-10', '05:30:00', '05:50:00'),
(7, 3, '2021-04-10', '02:00:00', '02:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `examples`
--

CREATE TABLE `examples` (
  `exID` int(11) NOT NULL,
  `exName` varchar(255) DEFAULT NULL,
  `exAbout` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examples`
--

INSERT INTO `examples` (`exID`, `exName`, `exAbout`) VALUES
(4, 'qq', 'qq'),
(5, 'qq', 'qq'),
(7, 'ooooooo', 'lllllllll'),
(8, 'ooo', 'yyyy'),
(10, 'ooo', 'yyyy'),
(160, 'qq', 'qq'),
(161, 'hihi', 'hahaha');

-- --------------------------------------------------------

--
-- Table structure for table `therapists`
--

CREATE TABLE `therapists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `therapists`
--

INSERT INTO `therapists` (`id`, `name`) VALUES
(1, 'John'),
(2, 'Arnold'),
(3, 'Robert'),
(4, 'Ervin'),
(5, 'Smith');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(10) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `imageName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `firstName`, `middleName`, `lastName`, `gender`, `birthdate`, `email`, `password`, `imageName`) VALUES
(1, 'John Carlo', 'Santiago', 'Carbonel', 'Male', '1999-04-09', 'jc.carbonel@gmail.com', '12345', 'user_images_sql/black_clover.jpg'),
(2, 'John Elrom', 'Verbo', 'Baylon', 'Male', '1999-10-13', 'je.baylon@gmail.com', '12345', 'user_images_sql/slime.jpg'),
(3, 'Mark Angelo', 'Alvarez', 'Martillano', 'Male', '1999-05-19', 'ma.martillano@gmail.com', '12345', 'user_images_sql/world-trigger.jpeg'),
(4, 'John', 'David', 'Penetrante', 'Male', '2021-04-08', 'jd.penetrante@gmail.com', '12345', 'user_images_sql/Black-Clover-2.jpg'),
(5, 'Charles', 'Gaffud', 'Laurente Jr', 'male', '1999-01-06', 'cgj@gmail.com', '12345', 'user_images_sql/www.YTS.AM.jpg'),
(6, 'Louise Mark Kit', 'Soriano', 'Geronimo', 'male', '1997-04-15', 'lmkg@gmail.com', '12345', 'user_images_sql/currency.jpg'),
(7, 'Brendan Keith', 'Queral', 'Dela Cruz', 'male', '1999-06-20', 'bkd@gmail.com', '12345', 'user_images_sql/hands-grip.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advancedsql`
--
ALTER TABLE `advancedsql`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examples`
--
ALTER TABLE `examples`
  ADD PRIMARY KEY (`exID`);

--
-- Indexes for table `therapists`
--
ALTER TABLE `therapists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advancedsql`
--
ALTER TABLE `advancedsql`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `examples`
--
ALTER TABLE `examples`
  MODIFY `exID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `therapists`
--
ALTER TABLE `therapists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
