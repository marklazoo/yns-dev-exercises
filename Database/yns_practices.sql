-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2021 at 04:05 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yns_practices`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer_value`) VALUES
(1, 1, 'Yuki Tabata'),
(2, 1, 'Yuko Shimizu'),
(3, 1, 'Katsuya Terada'),
(4, 2, 'Crimson Lion'),
(5, 2, 'Black Bulls'),
(6, 2, 'Green Mantis'),
(7, 3, '18'),
(8, 3, '16'),
(9, 3, '15'),
(10, 4, 'Yami Sukehiro'),
(11, 4, 'Nozel Silva'),
(12, 4, 'Jack the Ripper'),
(13, 5, 'Anti-Magic'),
(14, 5, 'Time Magic'),
(15, 5, 'Spatial Magic'),
(16, 6, 'Morning Moon'),
(17, 6, 'Dark Tria'),
(18, 6, 'Midnight Sun'),
(19, 7, 'cat'),
(20, 7, 'dog'),
(21, 7, 'eagle'),
(22, 8, '160'),
(23, 8, '170'),
(24, 8, '150'),
(25, 9, 'Diamond Kingdom'),
(26, 9, 'Heart Kingdom'),
(27, 9, 'Spade Kingdom'),
(28, 10, 'Liebe'),
(29, 10, 'Zenon'),
(30, 10, 'Gimodelo');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_value` varchar(255) NOT NULL,
  `answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_value`, `answer_id`) VALUES
(1, 'Who is the japanese illustrator of Black Clover?', 1),
(2, 'What magic knights squad accepts misfits or weird individuals as its members?', 5),
(3, 'What age did Yuno receive his four-leaf grimoire?', 9),
(4, 'Who among the captains uses dark magic?', 10),
(5, 'What magic does the wizard king Julius use?', 14),
(6, 'What was the name of the group of the reincarnated elves?', 18),
(7, 'What animal did Vanessa\'s magic of threads take shape?', 19),
(8, 'Black Clover ended last March 30, 2021 in what episode?', 23),
(9, 'The dark triad belongs to what kingdom?', 27),
(10, 'Licita adopted a demon and named him what?', 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
